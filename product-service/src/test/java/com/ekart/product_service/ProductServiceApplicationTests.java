package com.ekart.product_service;

import com.ekart.product_service.dto.ProductRequest;
import com.ekart.product_service.model.Product;
import com.ekart.product_service.repository.ProductRepository;
import com.google.gson.Gson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.github.dockerjava.core.MediaType;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//Integration Test using TestContainers and JUnit 5 Jupiter, here we have created a container called mongodb with a version
@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc

class ProductServiceApplicationTests {

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private MockMvc mockMvc;

	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:4.4.2");


	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry){
		dynamicPropertyRegistry.add("spring.data.mongodb.uri",mongoDBContainer::getReplicaSetUrl);
	}

	@Test
	void shouldCreateProducts() throws Exception {
		ProductRequest productRequest = getProductRequest();
		String productRequestString = new Gson().toJson(productRequest);
		mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
						.contentType(MediaType.APPLICATION_JSON.getMediaType())
						.content(productRequestString))
				.andExpect(status().isCreated());
        Assertions.assertEquals(1, productRepository.findAll().size());
	}

	@Test
	void shouldRetrieveAllProducts() throws Exception {
		saveProductToRepository("iPhone 15", "24MP camera and 4G", BigDecimal.valueOf(1300));
		saveProductToRepository("Samsung Galaxy", "48MP camera and 5G", BigDecimal.valueOf(1200));

		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/")
						.contentType(MediaType.APPLICATION_JSON.getMediaType()))
				.andExpect(status().isOk());
	}
	private ProductRequest getProductRequest() {
		return ProductRequest.builder()
				.name("iPhone 15")
				.description("24MP camera and 4G")
				.price(BigDecimal.valueOf(1300))
				.build();
	}

	private void saveProductToRepository(String name, String description, BigDecimal price) {
		Product product = new Product();
		product.setName(name);
		product.setDescription(description);
		product.setPrice(price);
		productRepository.save(product);
	}
}

