package com.ekart.order_service.service;

import com.ekart.order_service.dto.*;
import com.ekart.order_service.model.Order;
import com.ekart.order_service.model.OrderLineItems;
import com.ekart.order_service.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {
    private final WebClient.Builder webClientBuilder;
    private static final ModelMapper modelMapper = new ModelMapper();
    private final OrderRepository orderRepository;
    public void createOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());
        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDTOList().stream().map(this::mapToDTO).toList();
        order.setOrderLineItemsList(orderLineItems);
        List<String> skuCodes = order.getOrderLineItemsList().stream().map(OrderLineItems::getSkuCode).toList();

        InventoryResponse[] inventoryResponseArray = webClientBuilder.build().get().uri("http://inventory-service/api/inventory",
                        uriBuilder -> uriBuilder
                                .queryParam("skuCode",skuCodes)
                                .build()).retrieve()
                .bodyToMono(InventoryResponse[].class).block();
        assert inventoryResponseArray != null;
        boolean allProductInStock = Arrays.stream(inventoryResponseArray)
                .allMatch(InventoryResponse::getIsInStock);
        if(allProductInStock){
            orderRepository.save(order);
        }else {
            throw new IllegalArgumentException("Product is not in stock");
        }
    }

    private OrderLineItems mapToDTO(OrderLineItemsDTO orderLineItemsDTO) {
        OrderLineItems orderLineItems = new OrderLineItems();
        orderLineItems.setPrice(orderLineItemsDTO.getPrice());
        orderLineItems.setQuantity(orderLineItemsDTO.getQuantity());
        orderLineItems.setSkuCode(orderLineItemsDTO.getSkuCode());
        return orderLineItems;
    }

    public List<OrderResponse> getAllOrders() {
        List<Order> orders = orderRepository.findAll();
        return orders.stream()
                .map(this::mapToOrderResponse)
                .collect(Collectors.toList());
    }


    private OrderLineItems mapToOrderLineItemEntity(@NotNull OrderLineItemsDTO orderLineItemDTO) {
        return OrderLineItems.builder()
                .skuCode(orderLineItemDTO.getSkuCode())
                .price(orderLineItemDTO.getPrice())
                .quantity(orderLineItemDTO.getQuantity())
                .build();
    }

    private OrderResponse mapToOrderResponse(Order order) {
        return OrderResponse.builder()
                .id(order.getId())
                .orderNumber(order.getOrderNumber())
                .orderLineItems(order.getOrderLineItemsList().stream()
                        .map(this::mapToOrderLineItemResponse)
                        .collect(Collectors.toList()))
                .build();
    }

    private OrderLineItemResponse mapToOrderLineItemResponse(OrderLineItems orderLineItem) {
        return OrderLineItemResponse.builder()
                .id(orderLineItem.getId())
                .skuCode(orderLineItem.getSkuCode())
                .price(orderLineItem.getPrice())
                .quantity(orderLineItem.getQuantity())
                .build();
    }
}

