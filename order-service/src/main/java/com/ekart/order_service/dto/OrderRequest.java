package com.ekart.order_service.dto;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {


    private List<OrderLineItemsDTO> orderLineItemsDTOList;
}